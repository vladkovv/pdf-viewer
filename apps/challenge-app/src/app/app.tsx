import styles from './app.module.scss';
import { ChallengeEntry } from './challengeEntry';

export function App() {
  return (
    <div className={styles.app} style={{ height: "100%" }}>
        <ChallengeEntry />
    </div>
  );
}

export default App;
