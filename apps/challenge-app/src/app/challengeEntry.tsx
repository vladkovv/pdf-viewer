import { PDFViewer } from '@monorepo/components';

export const ChallengeEntry = () => {
  return (
    <div style={{ height: '100%' }}>
      <PDFViewer />
    </div>
  );
};
