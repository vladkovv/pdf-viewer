import {Dispatch, FC, SetStateAction, useCallback, useMemo, useState} from "react";
import { ListChildComponentProps } from 'react-window'
import { Page, PDFPageProxy } from "react-pdf";

import styles from "./page-item.module.scss";


interface PageProps {
  scale: number;
  containerWidth: number;
  setPageHeight: (index: number, height: number) => void;
  setRenderTime: Dispatch<SetStateAction<number | null>>;
}

export const PageItem: FC<ListChildComponentProps<PageProps>> = ({
  index,
  style,
  data: {
    scale,
    setPageHeight,
    containerWidth,
    setRenderTime
  }
}) => {
  const [originalPageWidth, setOriginalPageWidth] = useState<number | null>(null)
  const renderStart = performance.now()

  const onPageLoadSuccess = useCallback(({ originalWidth }: PDFPageProxy) => {
    setOriginalPageWidth(originalWidth)

  }, [])

  const onPageRenderSuccess = useCallback(({ _pageIndex, height }: PDFPageProxy) => {
    setPageHeight(_pageIndex, height)
    //measuring render time of the first page
    if (index === 0) {
      setRenderTime(performance.now() - renderStart)
    }
  }, [renderStart, setRenderTime, setPageHeight, index])

  const pageWidth = useMemo(() => {
    if(originalPageWidth && originalPageWidth < containerWidth) {
      return originalPageWidth
    }

    return containerWidth
  }, [containerWidth, originalPageWidth])

  return (
    <div style={style} className={styles.listItem}>
      <Page
        className={styles.page}
        pageNumber={index + 1}
        scale={scale}
        width={pageWidth}
        renderAnnotationLayer={false}
        onRenderSuccess={onPageRenderSuccess}
        onLoadSuccess={onPageLoadSuccess}
      />
    </div>
  )
}
