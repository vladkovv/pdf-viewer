import styles from './pdfinfo.module.scss'
import { FC } from "react";

interface PdfInfoProps {
  renderTime: number | null;
  url: string;
}

export const PdfInfo: FC<PdfInfoProps> = ({ renderTime, url }) => {

  const getRenderValue = () => {
    if(renderTime) {
      return `${Math.floor(renderTime)}ms`
    }

    return 'Loading...'
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.urlInfo}>
        <span className={styles.title}>LOADED URL</span>
        <label>{url}</label>
      </div>
      <div className={styles.renderInfo}>
        <span className={styles.title}>RENDER TIME</span>
        <span>{getRenderValue()}</span>
      </div>
    </div>
  )
}
