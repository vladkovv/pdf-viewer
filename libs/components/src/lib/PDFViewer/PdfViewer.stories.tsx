import { Story, Meta } from '@storybook/react';
import { PDFViewer, PDFViewerProps } from './index';

export default {
  component: PDFViewer,
  title: 'Components',
} as Meta;

const Template: Story<PDFViewerProps> = (args) => <PDFViewer {...args} />;

export const Viewer = Template.bind({});
Viewer.args = {};
