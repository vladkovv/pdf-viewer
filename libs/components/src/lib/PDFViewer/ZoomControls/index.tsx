import { FC } from "react";
import { ReactComponent as ZoomOut } from '../assets/zoom-out.svg'
import { ReactComponent as ZoomIn } from '../assets/zoom-in.svg'

import styles from './zoomcontrols.module.scss'

interface ZoomControlsProps {
  onZoomOut: () => void;
  onZoomIn: () => void;
}

export const ZoomControls:FC<ZoomControlsProps> = ({ onZoomOut, onZoomIn }) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.zoomButton} onClick={onZoomOut}>
        <ZoomOut />
      </div>
      <div className={styles.zoomButton} onClick={onZoomIn}>
       <ZoomIn />
      </div>
    </div>
  )
}
