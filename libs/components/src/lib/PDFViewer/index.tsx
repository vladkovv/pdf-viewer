import { FC, useCallback, useRef, useState } from "react";
import { Document, pdfjs } from "react-pdf";
import AutoSizer from 'react-virtualized-auto-sizer';
import { VariableSizeList } from "react-window";
import { usePinch } from "@use-gesture/react";

import { PdfInfo } from "./PdfInfo";
import { ZoomControls } from "./ZoomControls";
import { PageItem } from "./PageItem";
import pdf from "./StatementOfReturn.pdf";

import 'react-pdf/dist/esm/Page/TextLayer.css'
import styles from './PDFViewer.module.scss'

pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/legacy/build/pdf.worker.min.js`

const MIN_SCALE = 1
const MAX_SCALE = 5
const SCALE_STEP = 0.3


export interface PDFViewerProps {
  url?: string;
}

export const PDFViewer: FC<PDFViewerProps> = ({ url = pdf }) => {
  const [numPages, setNumPages] = useState<number | null>(null);
  const [scale, setScale] = useState<number>(1);
  const [renderTime, setRenderTime] = useState<number | null>(null);

  const docRef = useRef<HTMLDivElement>(null);
  const listRef = useRef<any>();

  const pagesHeights = useRef<Record<string, number>>({})

  const onDocumentLoadSuccess = ({ numPages }: pdfjs.PDFDocumentProxy) => {
    setNumPages(numPages);
  };

  const getPageSize = useCallback((index: number) => {
    return pagesHeights.current[index] || 840;
  }, [pagesHeights])

  const setPageHeight = useCallback((index: number, size: number) => {
    listRef.current.resetAfterIndex(0);
    pagesHeights.current[index] = size;
  }, [listRef, pagesHeights])

  const handleZoomIn = useCallback(() => {
    if (scale < MAX_SCALE) setScale(scale + SCALE_STEP);
  }, [scale]);

  const handleZoomOut = useCallback(() => {
    if (scale > MIN_SCALE) setScale(scale - SCALE_STEP);
  },[scale]);

  usePinch(({ offset: [d], event, last}) => {
    if (last) return

    if ((event as WheelEvent).deltaY === 100) {
      return handleZoomOut()
    } else if ((event as WheelEvent).deltaY  === -100) {
      return handleZoomIn()
    }

    setScale(d);
  }, { eventOptions: { passive: false }, scaleBounds: { min: 1, max: 5 }, target: docRef });

  return (
    <div className={styles.viewer}>
      <div className={styles.controls}>
        <PdfInfo renderTime={renderTime} url={url} />
        <ZoomControls onZoomIn={handleZoomIn} onZoomOut={handleZoomOut} />
      </div>
      <div className={styles.documentWrapper} ref={docRef}>
        <Document file={url} className={styles.document} onLoadSuccess={onDocumentLoadSuccess}>
          <AutoSizer>
            {({ height, width }) => (
                <VariableSizeList
                  itemSize={getPageSize}
                  height={height}
                  itemCount={numPages || 0}
                  width={width}
                  ref={listRef}
                  itemData={{
                    scale,
                    setRenderTime,
                    setPageHeight,
                    containerWidth: width
                  }}
                >
                  {PageItem}
                </VariableSizeList>
              )
            }
          </AutoSizer>
        </Document>
      </div>
      </div>
  );
};
